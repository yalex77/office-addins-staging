function checkSignature(eventObj) {
  const userEmail = Office.context.mailbox.userProfile.emailAddress;
  const signatureUrl = 'https://newoldstamp.com/offices/signatures/' + userEmail;

  fetch(signatureUrl)
    .then(res => res.text())
    .then(signature => {
      if (signature.length) {
        Office.context.mailbox.item.body.setSignatureAsync(
          signature,
          {
            coercionType: "html",
            asyncContext: eventObj,
          },
          function (asyncResult) {
            asyncResult.asyncContext.completed();
          }
        )
      }
    }
  )
}

Office.actions.associate("checkSignature", checkSignature);

Office.initialize = function (reason) {};

